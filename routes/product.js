const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js")

// Create/Add Product
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send (resultFromController));

});

// Retrieve All Active Products
router.get("/", (req,res) => {
	productController.getAllActive().then(resultFromController => res.send (resultFromController));
})

// Retrieve a Single/Specific Product
router.get("/:productId", (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send (resultFromController));
})

// Update Product
router.put("/:productId/update", auth.verify, (req,res) => {
	const data = {
				product: req.body,
				isAdmin: auth.decode(req.headers.authorization).isAdmin
			}
	productController.updateProduct(req.params, data).then(resultFromController => res.send (resultFromController));
})

// Archive Product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
				reqParams: req.params,
				isAdmin: auth.decode(req.headers.authorization).isAdmin
			}
	productController.archiveProduct(data).then(resultFromController => res.send (resultFromController));
	
});

module.exports = router;
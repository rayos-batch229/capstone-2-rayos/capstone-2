const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js")
const auth = require("../auth.js")

//Registration
router.post("/register", (req,res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Authentication
router.post("/login", (req,res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Create Order
router.post("/myOrder", auth.verify, (req,res) =>{

    const admin = {
        isAdmin:  auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id
    }

    userControllers.createOrder(req.body, admin).then(controllerResult => res.send(controllerResult))

})
// Retrieve User Details
router.get("/:userId", auth.verify, (req,res) => {
	userControllers.getUser(req.params).then(resultFromController => res.send (resultFromController));
})


// Set user as Admin(Admin)
router.put("/:userId/updateUser", auth.verify, (req,res) => {
	const data = {
				reqParams: req.params,
				isAdmin: auth.decode(req.headers.authorization).isAdmin
			}
	userControllers.userAdmin(data).then(resultFromController => res.send (resultFromController));
	
});

// Retrieve Orders
router.get("/:userId", auth.verify, (req,res) => {
	userControllers.retrieveOrders(req.params).then(resultFromController => res.send (resultFromController));
})

module.exports = router;
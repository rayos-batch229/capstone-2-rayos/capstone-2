const Product = require("../models/Product.js");

// Create/Add Product(Admin)
module.exports.addProduct = async (data) => {

	if (data.isAdmin) {

		let newProduct = new Product({
			name : data.product.name,
			brand: data.product.brand,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {

			if (error) {
				return false;

			} else {
				return "Product has been Added!";
			};
		});

	} else {
		return "Authorized Personnel Only!";
	};
};

// Retrieve All Active Products
module.exports.getAllActive = () => {

	return Product.find({isActive : true}).then(result => {

		return result;
	});
}

// Retrieve Single/Specific Product
module.exports.getProduct = (reqParams) => {

	console.log(reqParams);

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
}

// Update Product(Admin)
module.exports.updateProduct = async (reqParams, data) => {
	if(data.isAdmin){
	let updatedProduct = {
		name: data.product.name,
		brand: data.product.brand,
		description: data.product.description,
		price: data.product.price
	};

	return await Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

		if(error){

			return false;

		} else {

			return "Product Updated!";
		};
	 });

	} else {
		return "Authorized Personnel Only!";
	};
};


// Archive Product(Admin)
module.exports.archiveProduct = async (data) => {

if(data.isAdmin){

		let updateActiveField = {
			isActive : false,
		};

		return Product.findByIdAndUpdate(data.reqParams.productId, updateActiveField).then((product, error) => {

			if (error) {

				return false;

			} else {

				return "Product Archived Successfully!";

			}
		});
		
	} else {

		return "Request Denied!";
	}
};


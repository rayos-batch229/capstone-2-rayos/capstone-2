const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
    	lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		} else {
			return "Successfully Registered!";
		}
	})

}

// Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}		
		}
	})

}

// Create Order
module.exports.createOrder = async (reqBody, user) => {

    if(user.isAdmin){
        return ("For Customer Only!")
    } else {

        let products = await Product.findById(reqBody.productId)
        let {productName, productBrand, price} = products;

        let userOrders = await User.findById(user.userId).then(user => {

            let totalAmount = reqBody.quantity * price

            user.orders.push({
                products: {
                    productId: reqBody.productId,
                    productBrand: productBrand,
                    price: price,
                    quantity: reqBody.quantity
                    } 
                },
                {
                 totalAmount: totalAmount
                }
            )

            return user.save().then((user,err) =>{
                if(err){
                    return false
                } else {
                    return true
                }
            })
        })


    let productOrders = await Product.findById(reqBody.productId).then(product => {

            product.orders.push({userId: user.userId},{quantity: reqBody.quantity});

            return product.save().then((product, error) => {

                if(error){
                    return false;

                } else {
                    return true;
                }
            })
        })

        if(userOrders && productOrders){
            return ("Orderdered Successful");
        } else {
            return false;
        }

    } 

}

// Retrieve User Details
module.exports.getUser = (reqParams) => {

	console.log(reqParams);

	return User.findById(reqParams.userId).then(result => {
		return result;
	});
}

// Set user as Admin(Admin)
module.exports.userAdmin = async (data) => {

if(data.isAdmin){

		let updateUserField = {
			isAdmin : true,
		};

		return User.findByIdAndUpdate(data.reqParams.userId, updateUserField).then((user, error) => {

			if (error) {

				return false;

			} else {

				return "User Update Successfully!";

			}
		});
		
	} else {

		return "Request Denied!";
	}
};

// Retrieve Orders
module.exports.retrieveOrders = (admin) => {

	if(admin.isAdmin){

	return User.aggregate([{ $match : {isAdmin : false}},{ $group : {_id: "$orders" }}]).then(results => {

		return results
	})

	} else {
		return ("Authorized Personnel only!")
	}
}